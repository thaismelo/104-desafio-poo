package com.company;

public class Retangulo extends Forma {
    private double altura, base;

    public Retangulo(double altura, double base) {
        this.altura = altura;
        this.base = base;
    }

    @Override
    public String toString() {
        return "Área do Retângulo calculado é = " + this.area;
    }

    @Override
    public void calculoArea() {
        this.area =  altura * base;
    }
}
