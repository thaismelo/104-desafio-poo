package com.company;

public class Triangulo extends Forma {
    private double ladoA, ladoB, ladoC;

    public Triangulo(double ladoA, double ladoB, double ladoC) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }

    @Override
    public String toString() {
        return "Área do Triângulo calculado é = " + this.area;
    }

    @Override
    public void calculoArea() {
        if (this.verificarLados()) {
            double s = (ladoA + ladoB + ladoC) / 2;
            this.area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
        }
    }

    public boolean verificarLados() {
        if((this.ladoA + this.ladoB) > this.ladoC && (this.ladoA + this.ladoC) > this.ladoB && (this.ladoB + this.ladoC) > this.ladoA){
            return true;
        } else {
            return false;
        }
    }
}
