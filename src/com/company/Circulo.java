package com.company;

public class Circulo extends Forma {
    private double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    public String toString() {
        return "Área do Circulo calculado é = " + this.area;
    }

    @Override
    public void calculoArea() {
        this.area = Math.pow(raio, 2) * Math.PI;
    }
}
