package com.company;

import java.util.Scanner;

public class UI {
    private Scanner leitura = new Scanner(System.in);
    private Util util = new Util();
    private boolean iniciarjogo;
    private double verificarOpcaoInicio, verificarOpcaoJogo;

    public void init() {
        do {
            util.mensagemUsuario("Ola! :3 Qual a forma geometrica deseja calcular a área? ");
            util.mensagemUsuario("Digite 1 para Circulo:");
            util.mensagemUsuario("Digite 2 para Retângulo:");
            util.mensagemUsuario("Digite 3 para Triângulo:");
            util.mensagemUsuario("Ou digite 0 para sair...");

            int entrada = leitura.nextInt();
            if (entrada > 0) {
                Util util = new Util();
                util.identificarFormaGeometrica(entrada);
            } else {
                return;
            }

            System.out.println("Deseja continuar jogando? Digite 1 para SIM e 0 para NÃO!");

            verificarOpcaoInicio = leitura.nextInt();
            iniciarjogo = verificarOpcaoInicio != 1 ? false : true;
        } while (iniciarjogo);
    }
}
