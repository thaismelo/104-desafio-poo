package com.company;
import java.util.Scanner;

public class Util {
    private Scanner leitura = new Scanner(System.in);
    private Triangulo triangulo;
    private Retangulo retangulo;
    private Circulo circulo;

    public static void mensagemUsuario(String mensagem){
        System.out.println(mensagem);
    }

    public Circulo solicitarEntradaCirculo(){
        Double raio;

        mensagemUsuario("Informe o valor do raio:");
        raio = leitura.nextDouble();

        circulo = new Circulo(raio);
        return circulo;
    }

    public Retangulo solicitarEntradaRetangulo(){
        Double altura, base;

        mensagemUsuario("Informe a altura de seu Retângulo:");
        altura = leitura.nextDouble();
        mensagemUsuario("Informe a base de seu Retângulo:");
        base = leitura.nextDouble();

        retangulo = new Retangulo(altura, base);
        return retangulo;
    }

    public Triangulo solicitarEntradaTriangulo(){
        Double ladoA, ladoB, ladoC;

        mensagemUsuario("Informe o primeiro lado do seu Triângulo:");
        ladoA = leitura.nextDouble();
        mensagemUsuario("Informe o segundo lado do seu Triângulo:");
        ladoB = leitura.nextDouble();
        mensagemUsuario("Informe o terceiro lado do seu Triângulo:");
        ladoC = leitura.nextDouble();

        triangulo = new Triangulo(ladoA, ladoB, ladoC);
        return triangulo;
    }

    public void identificarFormaGeometrica(int forma) {
        if (forma == 3) {
            this.solicitarEntradaTriangulo();

            if (triangulo.verificarLados()) {
                triangulo.calculoArea();
                mensagemUsuario(triangulo.toString());
            } else {
                mensagemUsuario("Houve algum problema na entrada dos valores do Triângulo, tente novamente...");
                this.solicitarEntradaTriangulo();
            }
        } else if (forma == 2) {
            this.solicitarEntradaRetangulo();
            retangulo.calculoArea();

            mensagemUsuario(retangulo.toString());
        } else {
            this.solicitarEntradaCirculo();
            circulo.calculoArea();

            mensagemUsuario(circulo.toString());
        }
    }
}
